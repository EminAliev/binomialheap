public class BinomialHeap {
    private Bnode head;

    public BinomialHeap() {
        head = null;
    }

    public void insert(Bnode x) {
        BinomialHeap h = new BinomialHeap();
        h.head = x;
        BinomialHeap newH = this.meld(h);
        head = newH.head;
    }

    public boolean isEmpty() {
        return head == null;
    }


    private void pair(Bnode y, Bnode z) {
        y.p = z;
        y.sibling = z.child;
        z.child = y;
        z.degree++;
    }

    public BinomialHeap meld(BinomialHeap h2) {
        BinomialHeap h = new BinomialHeap();
        h.head = mergeRootList(this, h2);
        head = null;
        h2.head = null;

        if (h.head == null)
            return h;

        Bnode prevX = null;
        Bnode x = h.head;
        Bnode nextX = x.sibling;


        while (nextX != null) {

            if (x.degree != nextX.degree || (nextX.sibling != null && nextX.sibling.degree == x.degree)) {
                prevX = x;
                x = nextX;
            } else {
                if (x.key < nextX.key) {
                    x.sibling = nextX.sibling;
                    pair(nextX, x);
                } else {
                    if (prevX == null)
                        h.head = nextX;
                    else
                        prevX.sibling = nextX;

                    pair(x, nextX);
                    x = nextX;
                }
            }

            nextX = x.sibling;
        }

        return h;
    }


    private static Bnode mergeRootList(BinomialHeap h1, BinomialHeap h2) {
        if (h1.head == null)
            return h2.head;
        else if (h2.head == null)
            return h1.head;
        else {
            Bnode head;
            Bnode tail;
            Bnode h1Next = h1.head,
                    h2Next = h2.head;


            if (h1.head.degree <= h2.head.degree) {
                head = h1.head;
                h1Next = h1Next.sibling;
            } else {
                head = h2.head;
                h2Next = h2Next.sibling;
            }

            tail = head;


            while (h1Next != null && h2Next != null) {
                if (h1Next.degree <= h2Next.degree) {
                    tail.sibling = h1Next;
                    h1Next = h1Next.sibling;
                } else {
                    tail.sibling = h2Next;
                    h2Next = h2Next.sibling;
                }

                tail = tail.sibling;
            }

            if (h1Next != null)
                tail.sibling = h1Next;
            else
                tail.sibling = h2Next;

            return head;
        }
    }


    public int removeMin() {
        if (head == null)
            return -1;

        Bnode x = head;
        Bnode y = x.sibling;
        Bnode pred = x;
        Bnode xPred = null;

        while (y != null) {
            if (y.key < x.key) {
                x = y;
                xPred = pred;
            }
            pred = y;
            y = y.sibling;
        }
        if (x == head)
            head = x.sibling;
        else
            xPred.sibling = x.sibling;


        BinomialHeap h = new BinomialHeap();

        Bnode z = x.child;
        while (z != null) {
            Bnode next = z.sibling;
            z.sibling = h.head;
            h.head = z;
            z = next;
        }
        BinomialHeap newH = this.meld(h);
        head = newH.head;
        return x.vertex;
    }

    public void decreaseKey(int vertex, int k, Bnode[] dist) {
        Bnode x = dist[vertex];
        x.key = k;
        Bnode y = x;
        Bnode z = y.p;

        while (z != null && (y.key < z.key)) {

            int v = y.key;
            y.key = z.key;
            z.key = v;
            v = y.vertex;
            y.vertex = z.vertex;
            z.vertex = v;


            dist[z.vertex] = z;
            dist[y.vertex] = y;

            y = z;
            z = y.p;
        }
    }
}

class Bnode {

    public int key;


    public int vertex;


    public Bnode p;


    public Bnode child;


    public Bnode sibling;

    public int degree;

    public Bnode(int vertex, int weight) {
        key = weight;
        this.vertex = vertex;
        p = null;
        child = null;
        sibling = null;
        degree = 0;
    }
}