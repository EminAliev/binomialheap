public class GenerationTest {
    public static void insertGeneration() {
        BinomialHeap b = new BinomialHeap();
        long before0 = System.currentTimeMillis();
        long before1 = System.nanoTime();
        for (int j = 0; j < 100; j++) {
            Bnode[] t0;
            int rand = j * 1000;
            t0 = new Bnode[rand];
            for (int i = 0; i < rand; i++) {
                t0[i] = new Bnode(i, (int) Math.round(10000 * Math.random()));
                b.insert(t0[i]);
            }
            System.out.println("insert number: " + j);
            System.out.println(before0 + System.currentTimeMillis());
            System.out.println(before1 + System.nanoTime());
        }
    }
    public static void removeMinGenereation() {
        BinomialHeap b = new BinomialHeap();
        long before0 = System.currentTimeMillis();
        long before1 = System.nanoTime();
        for (int j = 0; j < 100; j++) {
            Bnode[] t0;
            int rand = j * 1000;
            t0 = new Bnode[rand];
            for (int i = 0; i < rand; i++) {
                t0[i] = new Bnode(i, (int) Math.round(10000 * Math.random()));
                b.removeMin();
            }
            System.out.println("remove min number: " + j);
            System.out.println(before0 + System.currentTimeMillis());
            System.out.println(before1 + System.nanoTime());
        }
    }
}
